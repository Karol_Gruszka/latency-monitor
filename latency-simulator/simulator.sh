#!/bin/bash


latency="0ms"
network_interface="$2"
# whether you want to add or delete the delay
mode="$3"

if [ -z "$network_interface" ]
    then
        echo "You need to specify network interface on which you want to apply the delay."
        exit
fi

case $1 in 
    "low") latency="100ms";;
    "medium") latency="500ms";;
    "high") latency="1000ms";;
    "none") latency="0ms";;
    *) 
    echo "You need to specify for how long you want to delay traffic."
    exit
esac

if [ -z "$mode" ]
    then
        echo "Default mode has been set - we are going to add $latency of delay to the $network_interface network interface."
        mode="add"

    else
        echo "Non default mode has been set - we are going to delete $latency of delay from the $network_interface network interface."
        mode="del"
fi

sudo tc qdisc $mode dev $network_interface root netem delay $latency
