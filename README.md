# Requirements
- System with [tc](https://man7.org/linux/man-pages/man8/tc.8.html) tool installed
- Bash
- Docker compose
- Docker engine, release 19.03.0+

# How to use latency-simulator
```
chmod +x ./latency-simulator/simulator.sh
sudo ./latency-simulator/simulator.sh $latency $network_interface $mode
```

latency - delay that will be applied to the requests in the given network interface.
Available variants:
  - none = 0ms
  - low = 100ms
  - medium = 500ms
  - high = 1000ms

network_interface - interface that communicates with the outside world to which requests we will apply the delay. 
Telegraf's ping plugin pings few DNS servers, so we must assure that we specify the right interface.
On unix servers it can be done with e.g `ifconfig` command.

mode - whether we want to add the delay or delete it. To delete use "del" e.g `sudo ./latency-simulator/simulator.sh low eth0 del`, to add leave it empty e.g  `sudo ./latency-simulator/simulator.sh low eth0`


# How to use latency-monitor
`cd ./latency-monitor` and run `docker-compose up` to turn on the containers. 
Grafana will be available on `http://localhost:3000`. 
Dashboard with all of the metrics will be loaded into the container and available in the dashboards list under the name `Telegraf: system dashboard`.